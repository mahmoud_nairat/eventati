<?php

namespace App\Http\Controllers;

use Illuminate\View\View;

class HomeController extends Controller
{
    public function index()
    {
        return view('Public.templates.home');
    }
    public function home()
    {
        return view('Public.templates.home_main');
    }
}
