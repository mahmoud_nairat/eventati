<section id="intro" class="container" style="background-image: url('{{URL::to($organiser->cover_path)}}')">
    <div class="row">
        <div class="col-md-12">
            <div class="organiser_logo">
                <div class="thumbnail">
                    <img src="{{URL::to($organiser->full_logo_path)}}" />
                </div>
            </div>
            <h1>{{$organiser->name}}</h1>
            @if($organiser->about)
            <div class="description pa10">
                {!! $organiser->about !!}
            </div>
            @endif
        </div>
    </div>
</section>
<style>
    .organiser_logo{
        position: absolute;
        right: 20px;
        top: 0;
    }
    .organiser_logo .thumbnail{
        width: 100px;
    }
</style>